export class Artdata {
	"ART_ID":string;
	"AUTHOR_ID":string;
	"FORM_ID":string;
	"LOCATION_ID":string;
	"SCHOOL_ID":string;
	"TIMEFRAME_ID":string;
	"TYPE_ID":string;
	"TITLE":string;
	"DATE":string;
	"TECHNIQUE":string;
	"URL":string;
	"AUTHOR":string;
	"FORM":string;
	"LOCATION":string;
	"SCHOOL":string;
	"TIMEFRAME":string;
	"TYPE":string;
}
